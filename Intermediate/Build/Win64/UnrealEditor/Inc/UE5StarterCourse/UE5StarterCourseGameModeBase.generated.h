// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE5STARTERCOURSE_UE5StarterCourseGameModeBase_generated_h
#error "UE5StarterCourseGameModeBase.generated.h already included, missing '#pragma once' in UE5StarterCourseGameModeBase.h"
#endif
#define UE5STARTERCOURSE_UE5StarterCourseGameModeBase_generated_h

#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_SPARSE_DATA
#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_RPC_WRAPPERS
#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUE5StarterCourseGameModeBase(); \
	friend struct Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUE5StarterCourseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE5StarterCourse"), NO_API) \
	DECLARE_SERIALIZER(AUE5StarterCourseGameModeBase)


#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUE5StarterCourseGameModeBase(); \
	friend struct Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUE5StarterCourseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE5StarterCourse"), NO_API) \
	DECLARE_SERIALIZER(AUE5StarterCourseGameModeBase)


#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUE5StarterCourseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUE5StarterCourseGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUE5StarterCourseGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUE5StarterCourseGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUE5StarterCourseGameModeBase(AUE5StarterCourseGameModeBase&&); \
	NO_API AUE5StarterCourseGameModeBase(const AUE5StarterCourseGameModeBase&); \
public:


#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUE5StarterCourseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUE5StarterCourseGameModeBase(AUE5StarterCourseGameModeBase&&); \
	NO_API AUE5StarterCourseGameModeBase(const AUE5StarterCourseGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUE5StarterCourseGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUE5StarterCourseGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUE5StarterCourseGameModeBase)


#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_12_PROLOG
#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_SPARSE_DATA \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_RPC_WRAPPERS \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_INCLASS \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_SPARSE_DATA \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE5STARTERCOURSE_API UClass* StaticClass<class AUE5StarterCourseGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UE5StarterCourse_Source_UE5StarterCourse_UE5StarterCourseGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
