// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE5StarterCourse/UE5StarterCourseGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUE5StarterCourseGameModeBase() {}
// Cross Module References
	UE5STARTERCOURSE_API UClass* Z_Construct_UClass_AUE5StarterCourseGameModeBase_NoRegister();
	UE5STARTERCOURSE_API UClass* Z_Construct_UClass_AUE5StarterCourseGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_UE5StarterCourse();
// End Cross Module References
	void AUE5StarterCourseGameModeBase::StaticRegisterNativesAUE5StarterCourseGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AUE5StarterCourseGameModeBase_NoRegister()
	{
		return AUE5StarterCourseGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_UE5StarterCourse,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering LOD WorldPartition DataLayers Utilities|Transformation" },
		{ "IncludePath", "UE5StarterCourseGameModeBase.h" },
		{ "ModuleRelativePath", "UE5StarterCourseGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUE5StarterCourseGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::ClassParams = {
		&AUE5StarterCourseGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUE5StarterCourseGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUE5StarterCourseGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUE5StarterCourseGameModeBase, 4028471865);
	template<> UE5STARTERCOURSE_API UClass* StaticClass<AUE5StarterCourseGameModeBase>()
	{
		return AUE5StarterCourseGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUE5StarterCourseGameModeBase(Z_Construct_UClass_AUE5StarterCourseGameModeBase, &AUE5StarterCourseGameModeBase::StaticClass, TEXT("/Script/UE5StarterCourse"), TEXT("AUE5StarterCourseGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUE5StarterCourseGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
