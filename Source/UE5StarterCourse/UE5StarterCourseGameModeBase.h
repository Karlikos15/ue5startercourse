// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE5StarterCourseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE5STARTERCOURSE_API AUE5StarterCourseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
